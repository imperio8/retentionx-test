import ApiFactory from './ApiFactory';
import { serialize } from './querySerialize';

const getFruits = (param, query) => {
	return new Promise((resolve, reject) => {
		ApiFactory.get(`fruit/${param}?${serialize(query)}`).then(
			(success) => resolve(success.data),
			(error) => reject(error)
		);
	});
};
const postFruit = (payload) => {
	return new Promise((resolve, reject) => {
		ApiFactory.put(`fruit`, payload).then(
			(success) => resolve(success.data),
			(error) => reject(error)
		);
	});
};

const FruitsAPI = { getFruits, postFruit };

export default FruitsAPI;
