/* eslint-disable max-len */
import Axios from 'axios';

const apiFactory = Axios.create({
	baseURL: 'https://cors-anywhere.herokuapp.com/https://fruityvice.com/api/',
	headers: {
		'Content-Type': 'application/json',
	},
});

export default apiFactory;
