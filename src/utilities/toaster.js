import toast from 'react-simple-toasts';

const success = (text, options = {}) => {
	toast(text, { position: 'right', className: 'toast toast-success', ...options });
};
const error = (text, options) => {
	toast(text, { position: 'right', className: 'toast toast-error', ...options });
};

const toaster = { success, error };

export default toaster;
