import React from 'react';
import Label from './label';

const ViewToggler = ({ activeView, setActiveView }) => {
	return (
		<div>
			<Label show={true}>View:</Label>

			<div className='view-toggler'>
				<div
					className={`item ${activeView === 'Table' ? 'active' : 'inactive'}`}
					onClick={() => setActiveView('Table')}
				>
					<i className='icon-table mr-2' />
					Table
				</div>
				<div
					className={`item ${activeView === 'Charts' ? 'active' : 'inactive'}`}
					onClick={() => setActiveView('Charts')}
				>
					<i className='icon-chart-bar mr-2' />
					Charts
				</div>
			</div>
		</div>
	);
};

export default ViewToggler;
