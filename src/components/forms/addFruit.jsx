import React, { useEffect, useState } from 'react';
import './AddFruitStyles.scss';
import Modal from '../modal';
import AppInput from '../appInput/appInput';

const AddFruitForm = ({ fruit: _fruit, onSubmit, show, onClose }) => {
	const [fruit, setFruit] = useState(_fruit);

	const onInput = (key, event) => {
		setFruit((prevState) => ({ ...prevState, [key]: event.target.value }));
	};

	const onNutritionInput = (key, event) => {
		setFruit((prevState) => ({
			...prevState,
			nutritions: { ...prevState.nutritions, [key]: parseFloat(event.target.value) },
		}));
	};

	const submitFruit = () => {
		onSubmit(fruit);
	};

	useEffect(() => {
		setFruit(_fruit);
	}, [_fruit]);

	const actions = (
		<div className='flex flex-row gap-2'>
			<button
				type='button'
				className='bg-success hover:bg-info text-white h-10 rounded-md w-full sm:w-32'
				onClick={submitFruit}
			>
				Submit
			</button>
			<button
				type='button'
				className='bg-primary hover:bg-secondary text-white h-10 rounded-md w-full sm:w-32'
				onClick={onClose}
			>
				Cancel
			</button>
		</div>
	);
	return !!fruit ? (
		<Modal title='Add Fruit' actions={actions}>
			<div className='flex flex-col gap-3'>
				<AppInput label='Name' value={fruit.name} onChange={(event) => onInput('name', event)} />
				<AppInput label='Family' value={fruit.family} onChange={(event) => onInput('family', event)} />
				<AppInput label='Genus' value={fruit.genus} onChange={(event) => onInput('genus', event)} />
				<AppInput label='Order' value={fruit.order} onChange={(event) => onInput('order', event)} />
				<div className='flex flex-row justify-between gap-3'>
					<AppInput
						label='Carbohydrates'
						type='number'
						step='0.01'
						min='0'
						value={fruit.nutritions.carbohydrates}
						onChange={(event) => onNutritionInput('carbohydrates', event)}
					/>
					<AppInput
						label='Protein'
						type='number'
						step='0.01'
						min='0'
						value={fruit.nutritions.protein}
						onChange={(event) => onNutritionInput('protein', event)}
					/>
				</div>
				<div className='flex flex-row justify-between gap-3'>
					<AppInput
						label='Fat'
						type='number'
						step='0.01'
						min='0'
						value={fruit.nutritions.fat}
						onChange={(event) => onNutritionInput('fat', event)}
					/>
					<AppInput
						label='Calories'
						type='number'
						step='0.01'
						min='0'
						value={fruit.nutritions.calories}
						onChange={(event) => onNutritionInput('calories', event)}
					/>
				</div>
				<div className='flex flex-row'>
					<AppInput
						label='Sugar'
						type='number'
						step='0.01'
						min='0'
						value={fruit.nutritions.sugar}
						onChange={(event) => onNutritionInput('sugar', event)}
					/>
				</div>
			</div>
		</Modal>
	) : null;
};

export default AddFruitForm;
