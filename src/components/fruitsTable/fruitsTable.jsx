import React from 'react';
import './FruitsTableStyles.scss';

const FruitsTable = ({ data }) => {
	return (
		<div className='fruits-table-wrapper'>
			<table className='table-auto fruits-table'>
				<thead>
					<tr>
						<th className='table-head-cell'>Name</th>
						<th>Family</th>
						<th>Genus</th>
						<th>Order</th>
						<th>Carbohydrates</th>
						<th>Protein</th>
						<th>Fat</th>
						<th>Calories</th>
						<th>Sugar</th>
					</tr>
				</thead>
				<tbody>
					{data.map((row) => (
						<tr key={row.id}>
							<td>{row.name}</td>
							<td>{row.family}</td>
							<td>{row.genus}</td>
							<td>{row.order}</td>
							<td>{row.nutritions.carbohydrates}</td>
							<td>{row.nutritions.protein}</td>
							<td>{row.nutritions.fat}</td>
							<td>{row.nutritions.calories}</td>
							<td>{row.nutritions.sugar}</td>
						</tr>
					))}
				</tbody>
			</table>
			{data && data.length ? null : <div className='no-data'>No Data</div>}
		</div>
	);
};

export default FruitsTable;
