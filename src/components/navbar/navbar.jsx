import React, { useContext } from 'react';
import './NavbarStyles.scss';
import { Switch } from '@headlessui/react';
import ThemeContext from '../../store/ThemeContext';

const Navbar = () => {
	const { theme, setTheme } = useContext(ThemeContext);

	const isEnabled = () => theme === 'Light';

	const changeTheme = (val) => {
		setTheme(val ? 'Light' : 'Dark');
	};

	return (
		<div className={'wrapper'}>
			<span className={'title'}>Fresh Fruits Dashboard</span>
			<div className='flex items-center gap-2'>
				<span>{theme} Theme</span>
				<Switch
					checked={isEnabled()}
					onChange={changeTheme}
					className={`${isEnabled() ? 'bg-primary' : 'bg-secondary'}
          relative inline-flex flex-shrink-0 h-6 w-12 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus-visible:ring-2  focus-visible:ring-white focus-visible:ring-opacity-75`}
				>
					<span className='sr-only' />
					<span
						className={`${isEnabled() ? 'translate-x-6' : 'translate-x-0'}
            pointer-events-none inline-block h-5 w-5 rounded-full bg-white shadow-lg transform ring-0 transition ease-in-out duration-200`}
					/>
				</Switch>
			</div>
		</div>
	);
};

export default Navbar;
