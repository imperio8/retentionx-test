import React, { useState } from 'react';
import Dropdown from './dropdown/dropdown';
import AppInput from './appInput/appInput';

const PARAMS_LIST = [
	{ value: 'all', label: 'All' },
	{ value: 'nutrition', label: 'Nutrition' },
	{ value: 'family', label: 'Family' },
	{ value: 'genus', label: 'Genus' },
	{ value: 'order', label: 'Order' },
];

const NUTRIENTS_LIST = [
	{ value: 'carbohydrates', label: 'Carbohydrates' },
	{ value: 'protein', label: 'Protein' },
	{ value: 'fat', label: 'Fat' },
	{ value: 'calories', label: 'Calories' },
	{ value: 'sugar', label: 'Sugar' },
];

const Filter = ({ onSearch }) => {
	const [selectedParam, setSelectedParam] = useState(null);
	const [selectedNutrient, setSelectedNutrient] = useState(null);
	const [searchVal, setSearchVal] = useState('');
	const [range, setRange] = useState({ min: 0.01, max: 100 });

	const rangeChange = (key, val) => {
		setRange((prevState) => ({ ...prevState, [key]: val }));
	};

	const searchHandler = () => onSearch(selectedParam, selectedNutrient, searchVal, range);

	const canSearch = () => {
		return (
			(selectedParam && selectedParam.value !== 'nutrition' && searchVal) ||
			(selectedParam && selectedParam.value === 'nutrition' && selectedNutrient) ||
			(selectedParam && selectedParam.value === 'all')
		);
	};

	return (
		<div className='flex flex-row flex-wrap items-end gap-3'>
			<Dropdown
				label={'Select parameter'}
				options={PARAMS_LIST}
				optionLabel={'label'}
				selectedOption={selectedParam}
				onSelect={setSelectedParam}
				icon={'icon-params'}
			/>
			{selectedParam && selectedParam.value === 'nutrition' ? (
				<>
					<Dropdown
						label={'Select Nutrient'}
						options={NUTRIENTS_LIST}
						optionLabel={'label'}
						selectedOption={selectedNutrient}
						onSelect={setSelectedNutrient}
					/>
					<div className='flex flex-row w-full sm:w-56 gap-3'>
						<div className='flex-1'>
							<AppInput
								label='Min'
								type='number'
								step='0.01'
								min='0.01'
								value={range.min}
								onChange={(event) => rangeChange('min', event.target.value)}
							/>
						</div>
						<div className='flex-1'>
							<AppInput
								label='Max'
								type='number'
								step='0.01'
								min='0.02'
								value={range.max}
								onChange={(event) => rangeChange('max', event.target.value)}
							/>
						</div>
					</div>
				</>
			) : (
				<>
					{selectedParam && selectedParam.value !== 'all' ? (
						<div className='w-full sm:w-56'>
							<AppInput
								value={searchVal}
								onChange={(event) => setSearchVal(event.target.value)}
								label='Search criteria'
							/>
						</div>
					) : null}
				</>
			)}

			<div className='mt-5 sm:mt-0 w-full sm:w-32'>
				<button
					className={`${
						!canSearch() ? 'bg-primary cursor-not-allowed' : 'bg-accent hover:bg-main'
					} text-white h-10 rounded-md w-full sm:w-32`}
					disabled={!canSearch()}
					onClick={searchHandler}
				>
					<i className='icon-search w-10 mr-2' />
					<span>Search</span>
				</button>
			</div>
		</div>
	);
};

export default Filter;
