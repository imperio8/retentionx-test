import React, { useEffect, useState } from 'react';
import './FruitsChartsStyles.scss';
import { Bar, Pie } from 'react-chartjs-2';

const pieChartOptions = { maintainAspectRatio: true, plugins: { legend: { display: true } } };
const barChartOptions = { maintainAspectRatio: false, scales: { yAxes: [{ ticks: { beginAtZero: true } }] } };

const FruitsCharts = ({ data }) => {
	const [familyGroup, setFamilyGroup] = useState([]);
	const [genusGroup, setGenusGroup] = useState([]);
	const [orderGroup, setOrderGroup] = useState([]);
	const [bgColors, setBgColors] = useState([]);
	const [borderColors, setBorderColors] = useState([]);

	useEffect(() => {
		if (!data) return;
		const families = {};
		const genuses = {};
		const orders = {};
		data.forEach((fruit) => {
			families[fruit.family] = families[fruit.family] ? families[fruit.family] + 1 : 1;
			genuses[fruit.genus] = genuses[fruit.genus] ? genuses[fruit.genus] + 1 : 1;
			orders[fruit.order] = orders[fruit.order] ? orders[fruit.order] + 1 : 1;
		});
		setFamilyGroup(Object.entries(families).map(([key, val]) => ({ name: key, count: val })));
		setGenusGroup(Object.entries(genuses).map(([key, val]) => ({ name: key, count: val })));
		setOrderGroup(Object.entries(orders).map(([key, val]) => ({ name: key, count: val })));
	}, [data]);

	useEffect(() => {
		const _bgColors = [];
		const _borderColors = [];
		for (let step = 0; step < 20; step++) {
			_bgColors.push(randomColorGenerator());
			_borderColors.push(randomColorGenerator());
		}
		setBgColors(_bgColors);
		setBorderColors(_borderColors);
	}, []);

	const randomColorGenerator = () => '#' + Math.floor(Math.random() * 16777215).toString(16);

	const generatePieChartData = (group) => {
		const _data = {
			labels: [],
			datasets: [
				{
					data: [],
					backgroundColor: [],
					borderColor: [],
					borderWidth: 1,
				},
			],
		};
		group
			.sort((a, b) => b.count - a.count)
			.forEach((item, i) => {
				_data.labels.push(item.name);
				_data.datasets[0].data.push(item.count);
				_data.datasets[0].backgroundColor.push(bgColors[i]);
				_data.datasets[0].borderColor.push(borderColors[i]);
			});
		return _data;
	};

	const generateBarChartData = (group) => {
		const bars = [
			{ key: 'carbohydrates', color: '#60291b' },
			{ key: 'protein', color: '#06cc98' },
			{ key: 'fat', color: '#cfa63d' },
			{ key: 'calories', color: '#3e6cc7' },
			{ key: 'sugar', color: '#e37500' },
		];
		const _data = {
			labels: [],
			datasets: bars.map(({ key, color }) => ({
				label: key.toUpperCase(),
				data: [],
				backgroundColor: color,
			})),
		};
		group.forEach((item) => {
			_data.labels.push(item.name);
			_data.datasets[0].data.push(item.nutritions[bars[0]['key']]);
			_data.datasets[1].data.push(item.nutritions[bars[1]['key']]);
			_data.datasets[2].data.push(item.nutritions[bars[2]['key']]);
			_data.datasets[3].data.push(item.nutritions[bars[3]['key']]);
			_data.datasets[4].data.push(item.nutritions[bars[4]['key']]);
		});
		console.log(_data);
		return _data;
	};

	return (
		<div className='charts-wrapper'>
			<div className='flex gap-5'>
				<div className='card'>
					<div className='title'>Families</div>
					<div className='body'>
						{data && data.length ? null : <div className='no-data'>No Data</div>}
						<Pie options={pieChartOptions} data={generatePieChartData(familyGroup)} />
					</div>
				</div>
				<div className='card'>
					<div className='title'>Genuses</div>
					<div className='body'>
						{data && data.length ? null : <div className='no-data'>No Data</div>}
						<Pie options={pieChartOptions} data={generatePieChartData(genusGroup)} />
					</div>
				</div>
				<div className='card'>
					<div className='title'>Orders</div>
					<div className='body'>
						{data && data.length ? null : <div className='no-data'>No Data</div>}
						<Pie options={pieChartOptions} data={generatePieChartData(orderGroup)} />
					</div>
				</div>
			</div>
			<div className='bar-chart'>
				{data && data.length ? null : <div className='no-data'>No Data</div>}
				<Bar height={'460px'} data={generateBarChartData(data)} options={barChartOptions} />
			</div>
		</div>
	);
};

export default FruitsCharts;
