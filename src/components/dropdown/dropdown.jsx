import React, { Fragment } from 'react';
import Label from '../label';
import { Menu, Transition } from '@headlessui/react';
import './DropdownStyles.scss';

const Dropdown = ({ label, selectedOption, options, optionLabel, onSelect, icon }) => {
	return (
		<Menu as='div' className='menu-container'>
			<Label show={!!selectedOption}>
				<i className={`${icon} icon`} />
				{label}:
			</Label>
			<Menu.Button className={`menu-button ${selectedOption ? 'selected' : ''}`}>
				{selectedOption ? (
					selectedOption[optionLabel]
				) : (
					<span>
						<i className={`${icon} icon`} />
						{label}
					</span>
				)}
			</Menu.Button>
			<Transition
				as={Fragment}
				enter='transition ease-out duration-100'
				enterFrom='transform opacity-0 scale-95'
				enterTo='transform opacity-100 scale-100'
				leave='transition ease-in duration-75'
				leaveFrom='transform opacity-100 scale-100'
				leaveTo='transform opacity-0 scale-95'
			>
				<Menu.Items className='menu-items-container'>
					{options.map((param, i) => (
						<Menu.Item as='div' className='menu-item' key={i}>
							{({ active }) => (
								<button className={`button ${active ? 'active' : ''}`} onClick={() => onSelect(param)}>
									{param[optionLabel]}
								</button>
							)}
						</Menu.Item>
					))}
				</Menu.Items>
			</Transition>
		</Menu>
	);
};

export default Dropdown;
