import React from 'react';
import Label from '../label';
import './AppInputStyles.scss';

const AppInput = (props) => {
	const { label, hideLabel } = props;
	return (
		<div>
			{hideLabel ? null : <Label show={!!props.value}>{label}:</Label>}
			<input type='text' className='input' placeholder={label} {...props} />
		</div>
	);
};

export default AppInput;
