import React from 'react';

const Modal = ({ title, actions, children }) => {
	return (
		<div
			className='fixed z-10 inset-0 overflow-y-auto'
			aria-labelledby='modal-title'
			role='dialog'
			aria-modal='true'
		>
			<div className='flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0'>
				<div className='fixed inset-0 bg-background opacity-50 transition-opacity' aria-hidden='true' />
				<span className='hidden sm:inline-block sm:align-middle sm:h-screen' aria-hidden='true'>
					&#8203;
				</span>
				<div className='inline-block border align-bottom bg-background rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full'>
					<div className='text-lg bg-background leading-6 font-medium text-black' id='modal-title'>
						<div className='bg-border text-black p-4'>{title}</div>
						<div className='bg-background p-4 px-6'>{children}</div>
					</div>
					<div className='bg-border px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse'>{actions}</div>
				</div>
			</div>
		</div>
	);
};

export default Modal;
