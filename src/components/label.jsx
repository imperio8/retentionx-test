import { Transition } from '@headlessui/react';
import React from 'react';

const Label = ({ show, children }) => {
	return (
		<div className='h-7'>
			<Transition
				as={'div'}
				enter='transition ease-out duration-100'
				enterFrom='transform translate-x-10 translate-y-10 scale-95'
				enterTo='transform translate-x-0 translate-y-0 scale-100'
				leave='transition ease-in duration-75'
				leaveFrom='transform translate-x-0 translate-y-0 scale-100'
				leaveTo='transform translate-x-10 translate-y-10 scale-95'
				show={show}
			>
				<span className='text-accent'>{children}</span>
			</Transition>
		</div>
	);
};

export default Label;
