import React, { useContext, useEffect, useState } from 'react';
import './DashboardStyles.scss';
import Fruits from '../services/Fruits';
import Filter from '../components/filter';
import toaster from '../utilities/toaster';
import FruitsTable from '../components/fruitsTable/fruitsTable';
import ViewToggler from '../components/viewToggler';
import { Transition } from '@headlessui/react';
import LoadingContext from '../store/LoadingContext';
import FruitsCharts from '../components/fruitsCharts/fruitsCharts';
import AddFruitForm from '../components/forms/addFruit';

const DEFAULT_FRUIT = {
	name: '',
	family: '',
	genus: '',
	order: '',
	nutritions: {
		carbohydrates: null,
		protein: null,
		fat: null,
		calories: null,
		sugar: null,
	},
};

const Dashboard = () => {
	const [newFruit, setNewFruit] = useState(null);
	const [fruits, setFruits] = useState([]);
	const [activeView, setActiveView] = useState('Table');
	const { setLoading } = useContext(LoadingContext);

	const getFruits = (selectedParam, selectedNutrient, searchVal, range) => {
		setLoading(true);
		const params =
			selectedParam.value === 'all'
				? selectedParam.value
				: selectedParam.value === 'nutrition'
				? selectedNutrient.value
				: `${selectedParam.value}/${searchVal}`;

		const query = selectedParam.value === 'nutrition' && range;

		Fruits.getFruits(params, query)
			.then((res) => {
				console.log(res);
				setFruits(res);
			})
			.catch((e) => {
				toaster.error(e.response.data.error);
			})
			.finally(() => setLoading(false));
	};

	const submitFruit = (fruit) => {
		setLoading(true);
		Fruits.postFruit(fruit)
			.then((res) => {
				toaster.success(res.success);
				setNewFruit(null);
			})
			.catch((e) => toaster.error(e.response.data.error))
			.finally(() => {
				setLoading(false);
			});
	};

	useEffect(() => {
		setActiveView('Table');
	}, []);

	const toggleView = (val) => {
		if (val === activeView) return;
		setActiveView(false);
		setTimeout(() => {
			setActiveView(val);
		}, 300);
	};

	return (
		<>
			<AddFruitForm fruit={newFruit} onClose={() => setNewFruit(null)} onSubmit={submitFruit} />
			<div className={'dashboard-container'}>
				<div className='flex items-end gap-10'>
					<button
						className={`bg-success hover:bg-info text-white h-10 rounded-md w-full sm:w-32`}
						onClick={() => setNewFruit(DEFAULT_FRUIT)}
					>
						<i className='icon-plus w-10 mr-2' />
						<span>Add Fruit</span>
					</button>
					<ViewToggler activeView={activeView} setActiveView={toggleView} />
					<Filter onSearch={getFruits} />
				</div>
				<Transition
					as={'div'}
					style={{ height: '500px' }}
					enter='transform transition duration-300'
					enterFrom='scale-x-0'
					enterTo='scale-x-100'
					leave='transform duration-300 transition ease-in-out'
					leaveFrom='scale-x-100 '
					leaveTo='scale-x-0'
					show={!!activeView}
				>
					{activeView === 'Table' ? <FruitsTable data={fruits} /> : <FruitsCharts data={fruits} />}
				</Transition>
			</div>
		</>
	);
};

export default Dashboard;
