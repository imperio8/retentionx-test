import Dashboard from './views/dashboard';
import Navbar from './components/navbar/navbar';
import './assets/scss/App.scss';
import AppLoader from './components/appLoader/appLoader';
import { useState } from 'react';
import ThemeContext from './store/ThemeContext';
import LoadingContext from './store/LoadingContext';

function App() {
	const [theme, setTheme] = useState(localStorage.getItem('theme' || 'Light'));
	const [loading, setLoading] = useState(false);

	const setThemeHandler = (val) => {
		localStorage.setItem('theme', val);
		setTheme(val);
	};

	const themeValue = { theme, setTheme: setThemeHandler };
	const loadingValue = { loading, setLoading };

	const themeClass = () => (theme === 'Light' ? 'light-theme' : 'dark-theme');

	return (
		<LoadingContext.Provider value={loadingValue}>
			<ThemeContext.Provider value={themeValue}>
				<div className={`App ${themeClass()}`}>
					<Navbar />
					<Dashboard />
					<div className='text-center text-sm text-primary mb-2'>Developed with love by Fabian</div>
					{loading ? <AppLoader /> : null}
				</div>
			</ThemeContext.Provider>
		</LoadingContext.Provider>
	);
}

export default App;
