import React from 'react';

const ThemeContext = React.createContext({
	theme: 'Light',
	setTheme: () => {},
});
export default ThemeContext;
