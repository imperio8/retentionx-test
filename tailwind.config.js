module.exports = {
	purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
	darkMode: false, // or 'media' or 'class'
	theme: {
		extend: {
			colors: {
				primary: 'var(--primary)',
				secondary: 'var(--secondary)',
				accent: 'var(--accent)',
				background: 'var(--background)',
				dark: 'var(--dark)',
				info: 'var(--info)',
				border: 'var(--border)',
				success: 'var(--success)',
				main: 'var(--main)',
			},
		},
		textColor: (theme) => ({
			...theme('colors'),
			black: 'var(--text-normal)',
			inverted: 'var(--text-inverted)',
		}),
		borderColor: (theme) => ({
			...theme('colors'),
			DEFAULT: 'var(--border)',
		}),
		// backgroundColor: (theme) => ({
		// 	...theme('colors'),
		// 	black: 'var(--text-normal)',
		// 	inverted: 'var(--text-inverted)',
		// }),
	},
	variants: {},
	plugins: [],
};
